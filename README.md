Doom-3-FHD-Resolution-Fix-No-Mods

For those who want to play Doom 3 Original not the BFG edition

1. Open the "DoomConfig.cfg" file. (usualy found under "C:\Program Files (x86)\DOOM 3\base")

2. Then you have to put it in custom mode.
Find were it says "r_mode" and change it to "r_mode -1".

3. Find "r_customHeight" and "r_customWidth".
Change it to your resolution.

Mine is:
"r_customHeight 1080"
"r_customWidth 1920"

4. Start the game
